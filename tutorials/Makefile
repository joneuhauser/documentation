# Makefile written by
# Colin Marquardt <colin@marquardt-home.de>, 2007.

ALL_TUTORIALS += advanced
ALL_TUTORIALS += basic
ALL_TUTORIALS += calligraphy
ALL_TUTORIALS += elements
ALL_TUTORIALS += interpolate
ALL_TUTORIALS += shapes
ALL_TUTORIALS += tips
ALL_TUTORIALS += tracing
ALL_TUTORIALS += tracing-pixelart

# The targets here go through all tutorial directories and see if make can be run there
# The Makefile for each tutorial will find out about the languages it can generate.

.PHONY: all
all:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && $(MAKE) all) || exit 1; \
	done

.PHONY: update-authors
update-authors:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && $(MAKE) update-authors) || exit 1; \
	done

.PHONY: html
html:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && $(MAKE) html) || exit 1; \
	done

.PHONY: svg
svg:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && $(MAKE) svg) || exit 1; \
	done

.PHONY: pot
pot:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && $(MAKE) pot) || exit 1; \
	done

.PHONY: po
po:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && $(MAKE) po) || exit 1; \
	done

.PHONY: images
images:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && $(MAKE) images MYLANG=$(MYLANG)) || exit 1; \
	done

.PHONY: allimages
allimages:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && $(MAKE) allimages) || exit 1; \
	done

.PHONY: pretty-xml
pretty-xml:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && $(MAKE) pretty-xml) || exit 1; \
	done

.PHONY: check-input
check-input:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && $(MAKE) check-input MYLANG=$(MYLANG)) || exit 1; \
	done

.PHONY: check-output
check-output:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && $(MAKE) check-output MYLANG=$(MYLANG)) || exit 1; \
	done

.PHONY: copy
copy: copy-svg copy-html

.PHONY: copy-html
copy-html:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && $(MAKE) copy-html) || exit 1; \
	done

.PHONY: copy-svg
copy-svg:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && $(MAKE) copy-svg) || exit 1; \
	done

.PHONY: clean
clean:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && $(MAKE) clean) || exit 1; \
	done

.PHONY: clean-export
clean-export: clean-html-export clean-svg-export

.PHONY: clean-html-export
clean-html-export:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && $(MAKE) clean-html-export) || exit 1; \
	done

.PHONY: clean-svg-export
clean-svg-export:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && $(MAKE) clean-svg-export) || exit 1; \
	done

.PHONY: clean-html
clean-html:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && $(MAKE) clean-html) || exit 1; \
	done

.PHONY: clean-svg
clean-svg:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && $(MAKE) clean-svg) || exit 1; \
	done

.PHONY: version
version:
	@$(MAKE) --no-print-directory -f Makefile.targets version

.PHONY: help
help:
	@$(MAKE) --no-print-directory -f Makefile.targets help
